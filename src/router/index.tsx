import { RouteObject } from "react-router-dom";
import {
  About,
  Account,
  Contact,
  Login,
  MovieTickets,
  Register,
  Service,
  Tech,
} from "pages";

import { Demo } from "demo";
import { PATH } from "constant";
import Home from "pages/Home";
import { AuthLayout, MainLayout } from "components/layouts";
import Detail from "pages/Detail";

export const router: RouteObject[] = [
  {
    path: "/demo",
    element: <Demo />,
  },
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: PATH.account,
        element: <Account />,
      },
      {
        path: PATH.about,
        element: <About />,
      },
      {
        path: PATH.service,
        element: <Service />,
      },
      {
        path: PATH.tech,
        element: <Tech />,
      },
      {
        path: PATH.ticket,
        element: <MovieTickets />,
      },
      {
        path: PATH.detail,
        element: <Detail />,
      },
      {
        path: PATH.contact,
        element: <Contact />,
      },
    ],
  },
  {
    element: <AuthLayout />,
    children: [
      {
        path: PATH.login,
        element: <Login />,
      },
      {
        path: PATH.register,
        element: <Register />,
      },
    ],
  },
];

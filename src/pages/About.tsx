import AboutTemplate from "components/templates/AboutTemplate";

export const About = () => {
  return <AboutTemplate />;
};

export default About;

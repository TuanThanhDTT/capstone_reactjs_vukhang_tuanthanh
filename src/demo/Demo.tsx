
export const Demo = () => {
  return (
    <div>
      <h2 className="text-lime-300 text-26">Demo Tailwind</h2>
      <p className="text-30 text-[#1234] ml-3 py-6 px-[15px] hover:text-red-400 transition-all ease-in-out duration-200">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, voluptas.</p>
    </div>
  )
}

export default Demo
import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyRapServices } from "services";

export const getMovieShowtimesThunk = createAsyncThunk('quanLyRap/getMovieShowTimesThunk', async (id: number, { rejectWithValue }) => {
    try {
        const data = await quanLyRapServices.getMovieShowtimes(id)
        return data.data.content
    } catch (error) {
        return rejectWithValue(error)
    }
})
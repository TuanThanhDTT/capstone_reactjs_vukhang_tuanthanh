import { createSlice } from "@reduxjs/toolkit";
import { Theater } from "types/QuanLyRap";
import { getMovieShowtimesThunk } from "./thunk";

type QuanLyRapInitialState = {
    movieShowtimes?: Theater
}

const initialState: QuanLyRapInitialState = {
    movieShowtimes: {
        heThongRapChieu: [],
        maPhim: null,
        tenPhim: null,
        biDanh: null,
        trailer: null,
        hinhAnh: null,
        moTa: null,
        maNhom: null,
        hot: null,
        dangChieu: null,
        sapChieu: null,
        ngayKhoiChieu: null,
        danhGia: null,
    }
}

const quanLyRapSlice = createSlice({
    name: 'quanLyRap',
    initialState,
    reducers: {},
    extraReducers(builder) {
        builder.addCase(getMovieShowtimesThunk.fulfilled, (state, { payload }) => {
            state.movieShowtimes = payload
        })
    },
})
export const { reducer: quanLyRapReducer, actions: quanLyRapActions } = quanLyRapSlice
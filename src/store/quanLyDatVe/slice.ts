import { createSlice } from "@reduxjs/toolkit"
import { ChairList, MovieInfo } from "types"
import { getTicketsThunk, postMovieTicketsThunk } from "./thunk"

type quanLyDatVeInitialState = {
    thongTinPhim?: MovieInfo,
    danhSachGhe?: ChairList[],
    chairBookings?: ChairList[],
}
const initialState: quanLyDatVeInitialState = {
    thongTinPhim: {
        maLichChieu: "",
        tenCumRap: "",
        tenRap: "",
        diaChi: "",
        tenPhim: "",
        hinhAnh: "",
        ngayChieu: "",
        gioChieu: "",
    },
    danhSachGhe: [],
    chairBookings: [],
}
const quanLyDatVeSlice = createSlice({
    name: "quanLyDatVe",
    initialState,
    reducers: {
        setChairBookings: (state,action) => {
            const index = state.chairBookings.findIndex(item => item.maGhe === action.payload.maGhe)
            if(index != -1){
                state.chairBookings.splice(index,1)
            }else{
                state.chairBookings.push(action.payload) 
            }
        },
        hoanTat: (state,action)=>{
            state.chairBookings = action.payload
        },
    },
    extraReducers(builder) {
        builder.addCase(getTicketsThunk.fulfilled, (state, {payload}) => {
            state.thongTinPhim = payload.thongTinPhim,
            state.danhSachGhe = payload.danhSachGhe
        })
        builder.addCase(postMovieTicketsThunk.fulfilled, (payload) => {
            return payload
        })
    },
})
export const {actions: quanLyDatVeActions, reducer: quanLyDatVeReducer} = quanLyDatVeSlice
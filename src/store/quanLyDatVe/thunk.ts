import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyDatVeServices } from "services/quanLyDatVe";
import { InfoChair, ListTicket } from "types";

export const getTicketsThunk = createAsyncThunk('quanLyDatVe/getTicketsThunk', async (payload:string,{rejectWithValue}) => {
try {
    const data = await quanLyDatVeServices.getMovieTickets(payload)
    return data.data.content
} catch (error) {
    return rejectWithValue(error)
}
})

export const postMovieTicketsThunk = createAsyncThunk('quanLyDatVe/postMovieTicketsThunk', async (payload:InfoChair<ListTicket>,{rejectWithValue}) => {
    try {
        const data = await quanLyDatVeServices.postMovieTickets(payload)
        console.log("data",data);
        return data.data
    } catch (error) {
        return rejectWithValue(error)
    }
    })
   

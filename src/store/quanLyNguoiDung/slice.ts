import { createSlice } from "@reduxjs/toolkit";
import { getUserThunk, loginThunk, updateUserThunk } from "./thunk";
import { ChairListUser, TicketInfo, User, UserInfo } from "types";

type QuanLyNguoiDungInitialState = {
  user?: User ;
  getUser?: UserInfo<TicketInfo<ChairListUser>>;
  accessToken?: string;
  isUpdatingUser?: boolean;
};

const initialState: QuanLyNguoiDungInitialState = {
  // user: JSON.parse(localStorage.getItem('USER')),
  user: {
    taiKhoan: "",
    hoTen: "",
    email: "",
    soDT: "",
    maNhom: "",
    maLoaiNguoiDung: "",
    accessToken: "",
  },
  getUser: {
    taiKhoan: "",
    hoTen: "",
    email: "",
    soDT: "",
    maNhom: "",
    maLoaiNguoiDung: "",
    accessToken: "",
    loaiNguoiDung: {
      maLoaiNguoiDung: "",
      tenLoai: "",
    },
    thongTinDatVe: [],
  },
  accessToken: localStorage.getItem("accessToken"),
  isUpdatingUser: false,
};
const quanLyNguoiDungSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {
    // xử lý action đồng bộ
    logout: (state) => {
      state.user = undefined;
      localStorage.removeItem("accessToken");
    },
  },
  extraReducers: (builder) => {
    // xử lý action bất đồng bộ (call API)
    builder
      .addCase(loginThunk.fulfilled, (state, { payload }) => {
        state.user = payload;
        //Lưu thông tin đăng nhập vào Localstorage
        if (payload) {
          // localStorage.setItem('USER', JSON.stringify(payload))
          localStorage.setItem("accessToken", payload.accessToken);
        }
      })
      .addCase(getUserThunk.fulfilled, (state, {payload}) => {
        state.getUser = payload
      })
      .addCase(updateUserThunk.pending, (state) => {
        state.isUpdatingUser = true;
      })
      .addCase(updateUserThunk.fulfilled, (state) => {
        state.isUpdatingUser = false;
      })
      .addCase(updateUserThunk.rejected, (state) => {
        state.isUpdatingUser = false;
      });
  },
});
export const {
  reducer: quanLyNguoiDungReducer,
  actions: quanLyNguoiDungActions,
} = quanLyNguoiDungSlice;

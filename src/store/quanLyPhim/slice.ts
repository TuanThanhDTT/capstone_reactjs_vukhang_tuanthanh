import { createSlice } from "@reduxjs/toolkit";
import { Banner, Movie } from "types";
import { getBannerListThunk, getMovieListThunk } from "./thunk";


type QuanLyPhimInitialState = {
    movieList: Movie[]
    bannerList: Banner[]
    isFetchingMovieList: boolean
}
const initialState: QuanLyPhimInitialState = {
    movieList: [],
    bannerList: [],
    isFetchingMovieList: false,
}
const quanLyPhimSlice = createSlice({
    name: 'quanLyPhim',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getMovieListThunk.pending, (state) => {
                state.isFetchingMovieList = true
            })
            .addCase(getMovieListThunk.fulfilled, (state, { payload }) => {
                state.movieList = payload
                state.isFetchingMovieList = false
            })
            .addCase(getMovieListThunk.rejected, (state) => {
                state.isFetchingMovieList = false
            })
            .addCase(getBannerListThunk.fulfilled, (state, { payload }) => {
                state.bannerList = payload
            })
    },
})

export const { reducer: quanLyPhimReducer, actions: quanLyPhimActions } = quanLyPhimSlice
import { apiInstance } from "constant";
import { ChairList, InfoChair, ListTicket, MovieInfo, Tickets } from "types";

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_DAT_VE_API,
})
export const quanLyDatVeServices = {
    getMovieTickets: (maLichChieu:string) => api.get<ApiResponse<Tickets<MovieInfo,ChairList>>>(`/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`),
    postMovieTickets: (thongTinVe:InfoChair<ListTicket>)=> api.post<InfoChair<ListTicket>>(`/DatVe`,thongTinVe)
}
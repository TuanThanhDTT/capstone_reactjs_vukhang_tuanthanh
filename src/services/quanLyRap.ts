import { apiInstance } from "constant"
import { Theater } from "types/QuanLyRap"

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_RAP_API,
})
export const quanLyRapServices = {
    getMovieShowtimes: (id: number) => api.get<ApiResponse<Theater>>(`LayThongTinLichChieuPhim?MaPhim=${id}`)
}
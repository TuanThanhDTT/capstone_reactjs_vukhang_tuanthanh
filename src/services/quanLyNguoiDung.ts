import { apiInstance } from 'constant'
import { AccountSchemaType, RegisterSchemaType } from 'schema'
import { LoginSchemaType } from 'schema/LoginSchema'
import { ChairListUser, TicketInfo, User, UserInfo } from 'types'
const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API
})

export const quanLyNguoiDungServices = {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    register: (payload: RegisterSchemaType) => api.post('/DangKy', payload),
    login: (payload: LoginSchemaType) => api.post<ApiResponse<User>>('/DangNhap', payload),
    getUser: () => api.post<ApiResponse<UserInfo<TicketInfo<ChairListUser>>>>('/ThongTinTaiKhoan'),
    updateUser: (payload: AccountSchemaType) => api.put('/CapNhatThongTinNguoiDung', payload),
}

import { Modal as ModalA, ModalProps as ModalPropsA } from 'antd'

type ModalProps = ModalPropsA & {

}

export const Modal = (props: ModalProps) => {
    return <ModalA {...props} />
}
export default Modal
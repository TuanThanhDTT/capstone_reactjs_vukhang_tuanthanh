import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { RootState, useAppDispatch } from "store";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
import { useSelector } from "react-redux";
import { useAuth } from "hooks";

export const Header = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { user } = useAuth();
  const { getUser } = useSelector((state: RootState) => state.quanLyNguoiDung);
  return (
    <Headers>
      <div className="header-content">
        <a className="text-logo cursor-pointer" onClick={() => navigate("/")}>
          CYBER CINEMA
        </a>
        <div className="flex justify-around">
          <NavLink to={PATH.home} className="mx-20 uppercase nav-link">
            trang chủ
          </NavLink>
          <NavLink className="mx-20 uppercase nav-link" to={PATH.about}>
            giới thiệu
          </NavLink>
        </div>
        {user ? (
          <Popover
            content={
              <div>
                <h2 className="font-600 mb-10 p-10">Hi! {getUser?.hoTen}</h2>
                <hr />
                <div
                  className="!p-10 !mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                  onClick={() => {
                    navigate(PATH.account);
                  }}
                >
                  Thông tin tài khoản
                </div>
                <div
                  className="p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                  onClick={() => {
                    dispatch(quanLyNguoiDungActions.logout());
                    // navigate("/login");
                  }}
                >
                  Đăng xuất
                </div>
              </div>
            }
            trigger="click"
          >
            <Avatar
              className="!ml-40 !cursor-pointer !flex !items-center !justify-center"
              size={32}
              icon={<UserOutlined />}
              style={{ background: "grey" }}
            />
          </Popover>
        ) : (
          <div>
            <span
              className="cursor-pointer nav-link signIn"
              onClick={() => {
                navigate(PATH.login);
              }}
            >
              Đăng nhập
            </span>{" "}
            /{" "}
            <span
              className="cursor-pointer nav-link register"
              onClick={() => {
                navigate(PATH.register);
              }}
            >
              Đăng ký
            </span>
          </div>
        )}
      </div>
    </Headers>
  );
};

export default Header;

const Headers = styled.header`
  height: var(--header-height);
  background: var(--primary-color-1);
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  color: var(--primary-color-2);
  padding: 20px 0;
  top: 0;
  left: 0;
  position: fixed;
  width: 100%;
  z-index: 11;
  .header-content {
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    .nav-link {
      font-size: 0.75rem;
      font-weight: 700;
      transition: all 0.5s;
    }

    a:hover {
      color: #ff0e83;
    }
    span:hover {
      color: #ff0e83;
    }
    .ant-avatar:hover svg {
      color: #71f171;
    }
    .text-logo {
      color: #ff0e83;
      font-size: 2rem;
    }
  }
`;

import {
  Pagination as PaginationA,
  PaginationProps as PaginationPropsA,
} from "antd";

type PaginationProps = PaginationPropsA & {
  // props
};
export const Pagination = (props: PaginationProps) => {
  return <PaginationA {...props} />;
};

export default Pagination;

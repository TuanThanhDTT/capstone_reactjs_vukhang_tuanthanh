import { FacebookOutlined, YoutubeOutlined } from "@ant-design/icons";
import { styled } from "styled-components";

export const Footer = () => {
  return (
    <Footers className="">
      <div>
        <div className="footer__main grid grid-cols-4">
          <div className="footer__item">
            <h3>About Us</h3>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum,
              natus illo. Consequatur maiores veniam est.
            </p>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum,
              natus illo. Consequatur maiores veniam est.
            </p>
            <div className="footer__social flex gap-3">
              <FacebookOutlined className="item cursor-pointer text-40" />
              <YoutubeOutlined className="item cursor-pointer text-40" />
            </div>
          </div>
          <div className="footer__item">
            <h3>Working Time</h3>
            <div className="footer__time">
              <p style={{ marginTop: 0 }}>Monday</p>
              <p style={{ marginTop: 0 }}>9h30 - 18h30</p>
            </div>
            <div className="footer__time">
              <p>Tuesday</p>
              <p>9h30 - 18h30</p>
            </div>
            <div className="footer__time">
              <p>Wenesday</p>
              <p>9h30 - 18h30</p>
            </div>
            <div className="footer__time">
              <p>Thursday</p>
              <p>9h30 - 18h30</p>
            </div>
            <div className="footer__time">
              <p>Friday</p>
              <p>9h30 - 18h30</p>
            </div>
          </div>
          <div className="footer__item">
            <h3>Twitter Us</h3>
            <p>ultricies nec, pellentesque eu, pretium quis</p>
            <p>ultricies nec, pellentesque eu, pretium quis</p>
            <p>ultricies nec, pellentesque eu, pretium quis</p>
          </div>
          <div className="footer__item">
            <h3>Contact Us</h3>
            <form className="footer__subs">
              <input type="email" placeholder="name@gmail.com" />
              <button type="submit">Send</button>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Reiciendis quisquam ratione eveniet distinctio veniam
                consectetur mollitia inventore.
              </p>
            </form>
          </div>
        </div>
        <div className="footer__copyright">
          <p>
            © 2023 Lorem ipsum dolor sit amet. | Design by{" "}
            <span>CyberSoft</span>
          </p>
        </div>
      </div>
    </Footers>
  );
};

export default Footer;

const Footers = styled.footer`
  /* FOOTER START */
  margin-top: 40px;
  padding: 30px 0 20px 0;
  background: var(--primary-color-1);
  color: white;
  .footer__main {
    width: 80%;
    margin: 0 auto;
    padding-bottom: 30px;
    max-width: var(--max-width);
  }
  .footer__item {
    padding: 0 20px;
    h3 {
      font-size: 1.3rem;
      font-weight: 700;
      color: #ff0e83;
      margin-bottom: 1rem;
    }
    p {
      font-size: 1rem;
      line-height: 28px;
    }
    .footer__subs input {
      margin-bottom: 16px;
      padding: 10px;
      border-radius: 5px;
      border: 1px solid black;
      color: black;
    }
    .footer__subs input:focus {
      outline: none;
    }
    .footer__subs button {
      font-weight: 600;
      padding: 7px 16px;
      background-color: #ff0e83;
      border: solid 1px #fff;
      border-radius: 5px;
      margin-bottom: 24px;
      transition: all 0.5s;
    }
    button:hover {
      background-color: #fff;
      color: #212529;
      border-color: #ff0e83;
    }
  }
  .footer__social {
    .item {
      transition: all 0.5s;
      :hover {
        color: #ff0e83;
      }
    }
  }
  .footer__time {
    display: flex;
    justify-content: space-between;
    border-bottom: 1px dotted white;
    p {
      padding: 0;
      margin: 10px 0;
    }
  }
  .footer__copyright {
    text-align: center;
    font-size: 1rem;
    span {
      color: #ff0e83;
    }
  }
  /* FOOTER END */
`;

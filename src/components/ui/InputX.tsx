import { UseFormRegister } from "react-hook-form";
import { HTMLInputTypeAttribute } from "react";

// import React from 'react'
type InputProps = {
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
  type?: HTMLInputTypeAttribute;
  label?: string;
  className?: string;
  disabled?: boolean;
};
export const InputX = ({
  register,
  error,
  name,
  type,
  placeholder,
  label,
  className,
  disabled,
}: InputProps) => {
  return (
    <div className={className}>
      {label && <p className="mb-10">{label}</p>}
      <input
        disabled={disabled}
        type={type}
        placeholder={placeholder}
        {...register(name)}
        className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333]  focus:ring-blue-500 focus:border-blue-500 "
      />
      {error && <p className="text-red-500">{error}</p>}
    </div>
  );
};

export default InputX;

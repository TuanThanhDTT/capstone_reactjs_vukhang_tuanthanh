import { useSelector } from "react-redux";
import { RootState } from "store";
import styled from "styled-components";
import dayjs from "dayjs";

const TicketInfo = () => {
  const { getUser } = useSelector((state: RootState) => state.quanLyNguoiDung);
  console.log("user đã lấy data", getUser);
  console.log("thongTinDatVe", getUser?.thongTinDatVe);
  return (
    <Ticket className={getUser?.thongTinDatVe.length ? "h-auto" : "h-[400px]"}>
      <section className="text-gray-600 body-font">
        <div className="container mx-auto">
          <div className="flex flex-col text-center w-full mb-20">
            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
              LỊCH SỬ ĐẶT VÉ
            </h1>
            <p className="lg:w-2/3 mx-auto leading-relaxed text-base italic text-30">
              "Cyber Movie hi vọng quý khách sẽ được trải nghiệm cảm giác điện
              ảnh chưa từng có, và tận hưởng trọn vẹn các bom tấn đang chiếu tại
              cụm rạp!"
            </p>
          </div>
          <div className="flex flex-wrap -m-2">
            {getUser?.thongTinDatVe?.map((thongTinDatVe) => {
              return thongTinDatVe.danhSachGhe.map((ghe, index) => {
                const withTime24hourFormat = dayjs(thongTinDatVe.ngayDat);
                return (
                  <div className="p-2 lg:w-1/3 md:w-1/2 w-full" key={index}>
                    <div className="h-full flex items-center border p-4 rounded-lg border-purple-600">
                      <img
                        alt="team"
                        className="w-24 h-32 bg-gray-100 object-cover object-center flex-shrink-0 rounded-10 mr-4"
                        src={thongTinDatVe.hinhAnh}
                      />
                      <div className="flex-grow ml-10">
                        <h2 className="text-gray-900 title-font font-medium text-20">
                          {thongTinDatVe.tenPhim}
                        </h2>
                        <p className="text-gray-500 text-15">
                          Cụm rạp: {ghe.maHeThongRap}
                        </p>
                        <p className="text-gray-500 text-15">
                          Khu vực chiếu: {ghe.tenCumRap}
                        </p>
                        <p className="text-gray-500 text-15">
                          Vị trí ghế: {ghe.tenGhe}
                        </p>
                        <p className="text-gray-500 text-15">
                          Ngày đặt: {`${withTime24hourFormat}`}
                        </p>
                      </div>
                    </div>
                  </div>
                );
              });
            })}
          </div>
        </div>
      </section>
    </Ticket>
  );
};

export default TicketInfo;
const Ticket = styled.div`
  padding: 0 40px;
`;

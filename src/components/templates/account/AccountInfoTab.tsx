import { zodResolver } from "@hookform/resolvers/zod";
import { Button, InputX } from "components/ui";

import { useEffect } from "react";
import { toast } from "react-toastify";
import { useForm, SubmitHandler } from "react-hook-form";
import { AccountSchema, AccountSchemaType } from "schema";
import { RootState, useAppDispatch } from "store";
import { updateUserThunk } from "store/quanLyNguoiDung/thunk";
import { styled } from "styled-components";
import { useSelector } from "react-redux";

const AccountInfoTab = () => {
  // const { user } = useAuth();
  // console.log("user InfoTab", user);

  const dispatch = useAppDispatch();
  const { isUpdatingUser, getUser } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );
  // console.log("getUser", getUser);

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<AccountSchemaType>({
    mode: "onChange",
    resolver: zodResolver(AccountSchema),
  });

  useEffect(() => {
    reset({
      ...getUser,
      soDt: getUser?.soDT,
    });
  }, [getUser, reset]);

  const onSubmit: SubmitHandler<AccountSchemaType> = (value) => {
    dispatch(updateUserThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Cập nhật thông tin tài khoản thành công!");
      });
  };

  return (
    <form className="px-40 font-mono" onSubmit={handleSubmit(onSubmit)}>
      <InputS
        label="Tài khoản"
        name="taiKhoan"
        register={register}
        error={errors?.taiKhoan?.message}
      />
      <InputS
        label="Họ và tên"
        name="hoTen"
        register={register}
        error={errors?.hoTen?.message}
      />
      <InputS
        label="Số điện thoại"
        name="soDT"
        register={register}
        error={errors?.soDt?.message}
      />
      <InputS
        label="Mật khẩu"
        name="matKhau"
        register={register}
        error={errors?.matKhau?.message}
      />
      <InputS
        label="Email"
        name="email"
        register={register}
        error={errors?.email?.message}
      />
      <InputS
        label="Mã nhóm"
        name="maNhom"
        register={register}
        error={errors?.maNhom?.message}
      />
      <InputS
        label="Mã người dùng"
        name="maLoaiNguoiDung"
        register={register}
        error={errors?.maLoaiNguoiDung?.message}
      />
      <div className="text-right">
        <ButtonS
          loading={isUpdatingUser}
          htmlType="submit"
          className="mt-[10px] w-[200px] !h-[50px] font-mono"
          type="primary"
        >
          Lưu thay đổi
        </ButtonS>
      </div>
    </form>
  );
};

export default AccountInfoTab;

const InputS = styled(InputX)`
  margin-top: 16px;
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;

const ButtonS = styled(Button)`
  background-color: var(--primary-color-3) !important;
  border: white;
`;

import { Tabs } from "components/ui";
import AccountInfoTab from "./AccountInfoTab";
import { styled } from "styled-components";
import TicketInfo from "./TicketInfo";

export const AccountTemplate = () => {
  return (
    <div>
      <TabX>
        <div className="theater-box">
          <div className="flex justify-center">
            <div className="box-heading">
              <h2 className="text-heading font-mono">Thông tin tài khoản</h2>
            </div>
          </div>
          <TabS
            tabPosition="left"
            className="h-full"
            tabBarGutter={-5}
            type="card"
            items={[
              {
                label: (
                  <div className="font-mono text-left rounded-lg p-10">
                    Thông tin tài khoản
                  </div>
                ),
                key: "accountInfo",
                children: <AccountInfoTab />,
              },
              {
                label: (
                  <div className="font-mono text-left rounded-lg p-10">
                    Thông tin vé đã đặt
                  </div>
                ),
                key: "ticketInfo",
                children: <TicketInfo />,
              },
            ]}
          />
        </div>
      </TabX>
    </div>
  );
};

export default AccountTemplate;
const TabS = styled(Tabs)`
  transform: translateY(30px);
`;
const TabX = styled.div`
  width: 100vw;
  margin-left: calc(50% - 50vw);
  position: relative;
  .box-heading {
    margin-top: 20px;
    background-color: #f18720;
    display: inline-block;
    transform: skewX(-40deg);
    padding: 5px;
    width: 800px;
    .text-heading {
      text-align: center;
      text-transform: uppercase;
      font-size: 1.5rem;
      color: white;
      transform: skewX(40deg);
    }
  }
  .theater-box {
    max-width: 1300px;
    margin: auto;
  }
  .ant-tabs-nav-list {
    gap: 20px;
  }
  .ant-tabs-tab {
    background-color: rgb(0 0 0 / 50%) !important;
    border-radius: 10px !important;
    color: white;
    transition: all 0.5s;
    &:hover {
      background-color: #fecf06 !important;
      color: black !important;
    }
    &.ant-tabs-tab-active {
      background-color: #fecf06 !important;
      .ant-tabs-tab-btn {
        color: black !important;
      }
    }
  }
  .ant-tabs-content-holder {
    border: 0 !important;
  }
`;

import { ClockCircleOutlined } from "@ant-design/icons";
import { Tabs } from "components/ui";
import { PATH } from "constant";
import { NavLink, generatePath } from "react-router-dom";
import styled from "styled-components";

const Theater = ({ heThongRapChieu }) => {
  return (
    <TabX>
      <div className="theater-box">
        <div className="flex justify-center">
          <div className="box-heading">
            <h2 className="text-heading font-mono">hệ thống rạp</h2>
          </div>
        </div>
        <Tabs
          id="theater-tabs"
          tabPosition="left"
          tabBarGutter={-5}
          type="card"
          items={heThongRapChieu?.map((htrc) => ({
            label: (
              <div className="flex">
                <img
                  src={htrc.logo}
                  alt={htrc.tenHeThongRap}
                  style={{ height: "40px" }}
                  className="mr-3"
                />
                <span
                  className="font-500"
                  style={{ lineHeight: "35px", height: "35px" }}
                >
                  {htrc.tenHeThongRap}
                </span>
              </div>
            ),
            key: htrc.maHeThongRap,
            children: (
              <div>
                {htrc.cumRapChieu?.map((cumRap) => {
                  return (
                    <div key={cumRap.maCumRap}>
                      <div className="mt-5 flex flex-row">
                        <img
                          style={{ height: "50px", width: "50px" }}
                          src={cumRap.hinhAnh}
                          alt=""
                        />
                        <div className="ml-[30px]">
                          <p className="font-mono text-[var(--primary-color-3)] font-bold text-lg">
                            {cumRap.tenCumRap}
                          </p>
                          <p className="font-mono text-gray-400">
                            {cumRap.diaChi}
                          </p>
                        </div>
                      </div>
                      <div className="flex flex-row">
                        <p
                          className="text-white"
                          style={{ lineHeight: "50px", height: "50px" }}
                        >
                          <ClockCircleOutlined
                            style={{ transform: "translateY(-3px)" }}
                          />{" "}
                          Giờ chiếu
                        </p>
                        <div className="movie__showtimes grid grid-cols-4 w-[50%]">
                          {cumRap.lichChieuPhim?.map((lichChieu) => {
                            const path = generatePath(PATH.ticket, {
                              maLichChieu: lichChieu.maLichChieu,
                            });
                            return (
                              <NavLink
                                to={path}
                                className="time"
                                key={lichChieu.maLichChieu}
                              >
                                {new Date(
                                  lichChieu.ngayChieuGioChieu
                                ).toLocaleTimeString([], {
                                  hour: "2-digit",
                                  minute: "2-digit",
                                  hour12: false,
                                })}
                              </NavLink>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            ),
          }))}
        />
      </div>
    </TabX>
  );
};

export default Theater;

const TabX = styled.div`
  padding: 50px 0;
  margin-bottom: -40px;
  position: relative;
  width: 100%;
  background: var(--primary-color-1);
  &::before {
    content: "";
    background: url(../images/line-bg.png) repeat-x left top;
    background-size: contain;
    position: absolute;
    width: 100%;
    height: 2px;
    bottom: 0;
    left: 0;
  }
  .box-heading {
    background-color: #f18720;
    display: inline-block;
    transform: skewX(-40deg);
    padding: 5px;
    margin-bottom: 30px;
    width: 800px;
    .text-heading {
      text-align: center;
      text-transform: uppercase;
      font-size: 1.5rem;
      color: white;
      transform: skewX(40deg);
    }
  }
  .theater-box {
    max-width: 1300px;
    margin: auto;
  }
  .ant-tabs-nav-list {
    gap: 20px;
  }
  .ant-tabs-tab {
    background-color: rgb(0 0 0 / 50%) !important;
    border-radius: 10px !important;
    color: white;
    transition: all 0.5s;
    &:hover {
      background-color: #fecf06 !important;
      color: black !important;
    }
    &.ant-tabs-tab-active {
      background-color: #fecf06 !important;
      .ant-tabs-tab-btn {
        color: black !important;
      }
    }
  }
  .ant-tabs-content-holder {
    border: 0 !important;
  }
  .movie__showtimes {
    .time {
      color: #f18720;
      font-size: 30px;
      text-align: center;
      transition: all 0.5s;
      &:hover {
        color: #fecf06;
      }
    }
  }
`;

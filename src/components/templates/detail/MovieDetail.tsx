import { Button } from "components/ui";
import styled from "styled-components";

export const MovieDetail = ({ movieShowtimes, setOpen }) => {
  return (
    <MovieDetails>
      <div className="film-details-content">
        <div className="film-details-wrap">
          <div className="film-item t-2d grid grid-cols-12 gap-30">
            <div className="film-item-pic col-span-4">
              <img src={movieShowtimes?.hinhAnh} alt="" />
            </div>
            <div className="film-item-txt col-start-5 col-span-8">
              <h3 className="font-mono">{movieShowtimes?.tenPhim}</h3>
              <div className="film-overview font-mono">
                <span className="l-title">Khởi chiếu: </span>
                <span className="l-value">
                  {new Date(
                    Date.parse(movieShowtimes?.ngayKhoiChieu)
                  ).toLocaleDateString()}
                </span>
              </div>
              <p>{movieShowtimes?.moTa}</p>
              <div className="film-item-but">
                <Button
                  className="trailler-btn font-mono"
                  onClick={() => {
                    setOpen(true);
                  }}
                >
                  TRAILER
                </Button>
                {/* <Button className="cart-btn font-mono">MUA VÉ</Button> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </MovieDetails>
  );
};

export default MovieDetail;

const MovieDetails = styled.div`
  .film-details-content {
    position: relative;
    background: url(../images/film-bg.jpg) no-repeat center top;
    background-size: cover;
  }
  .film-details-wrap {
    position: relative;
    width: 100%;
    max-width: 1300px;
    display: block;
    margin: 0 auto;
    height: auto;
    .film-item {
      width: 100%;
      padding: 20px;
      overflow: hidden;
    }
    .film-item-pic {
      img {
        width: 100%;
        height: 450px;
        border: 9px solid #fae2a2;
        display: block;
      }
    }
    .film-item-but {
      position: relative;
      display: inline-block;
      text-align: center;
      width: 100%;
    }
    .trailler-btn {
      font-weight: normal;
      font-size: 24px;
      color: #fff;
      text-transform: uppercase;
      border-radius: 30px;
      -webkit-border-radius: 30px;
      /* line-height: 52px; */
      height: 52px;
      display: inline-block;
      vertical-align: middle;
      width: 180px;
      background: rgba(0, 0, 0, 0.5) url(/images/icon-play.png) no-repeat -14px center;
      text-align: left;
      padding-left: 52px;
      transition: all 0.3s ease-in-out;
      -webkit-transition: all 0.3s ease-in-out;
      cursor: pointer;
      margin: 5px 2px;
    }
    .cart-btn {
      background-color: #e00d7a;
      font-weight: normal;
      font-size: 24px;
      text-transform: uppercase;
      line-height: 30px;
      color: #fff;
      border-radius: 30px 0 30px 30px;
      -webkit-border-radius: 30px 0 30px 30px;
      line-height: 52px;
      padding: 0 20px;
      height: 52px;
      display: inline-block;
      vertical-align: middle;
      width: 146px;
      transition: all 0.3s ease-in-out;
      -webkit-transition: all 0.3s ease-in-out;
      cursor: pointer;
      margin: 5px 2px;
      box-shadow: 5px 5px 0 rgba(0, 0, 0, 0.1);
    }
    .film-item-txt {
      float: right;
      min-height: 450px;
      box-shadow: 10px 10px 10px 0 rgba(0, 0, 0, 0.3),
        0 0 50px 0 rgba(0, 0, 0, 0.4) inset;
      background-color: #f18720;
      padding: 30px 40px;
      h3 {
        font-weight: normal;
        font-size: 28px;
        text-transform: uppercase;
        line-height: 30px;
        color: #fff;
        text-align: left;
        margin-bottom: 30px;
      }
      p {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        color: #fff;
        line-height: 20px;
        text-align: left;
        margin-bottom: 10px;
      }
    }
    .film-overview {
      max-height: inherit;
      overflow: inherit;
      display: table;
      margin-bottom: 10px;
    }
    .l-title {
      display: table-cell;
      width: 100px;
      /* font-family: "MyriadLight"; */
      font-weight: normal;
      font-size: 16px;
      vertical-align: middle;
      color: #fff;
    }
    .l-value {
      display: table-cell;
      line-height: 16px;
      background-color: #fff;
      /* font-family: "MyriadBold"; */
      font-weight: normal;
      font-size: 20px;
      color: #000;
      border-radius: 20px 0 20px 0;
      -webkit-border-radius: 20px 0 20px 0;
      padding: 12px 30px;
      vertical-align: middle;
    }
  }
`;

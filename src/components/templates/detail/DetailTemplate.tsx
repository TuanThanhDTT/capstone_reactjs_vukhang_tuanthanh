import { Modal } from "components/ui";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getMovieShowtimesThunk } from "store/quanLyRap/thunk";
import { styled } from "styled-components";
import Theater from "./TheaterTab";
import MovieDetail from "./MovieDetail";

export const DetailTemplate = () => {
  const params = useParams();
  const { id } = params;
  const dispatch = useAppDispatch();
  const { movieShowtimes } = useSelector((state: RootState) => state.quanLyRap);
  useEffect(() => {
    dispatch(getMovieShowtimesThunk(Number(id))).unwrap();
  }, [id]);

  const [open, setOpen] = useState(false);

  return (
    <Details>
      <MovieDetail movieShowtimes={movieShowtimes} setOpen={setOpen} />
      <Theater heThongRapChieu={movieShowtimes?.heThongRapChieu} />
      <ModalVideos
        centered
        width={700}
        open={open}
        onOk={() => setOpen(false)}
        onCancel={() => setOpen(false)}
        cancelButtonProps={{ style: { 'display': 'none' } }}
        okButtonProps={{ style: { 'display': 'none' } }}
        closable={false}
      >
        <div id="small-dialog" className="zoom-anim-dialog mfp-hide">
          <iframe src={movieShowtimes.trailer} allowFullScreen style={{ height: '390px', width: '640px', margin: 'auto' }} />
        </div>
      </ModalVideos>
    </Details>
  );
};

export default DetailTemplate;

const Details = styled.div`
  width: 100vw;
  margin-left: calc(50% - 50vw);
`
const ModalVideos = styled(Modal)`
  #small-dialog {
    max-width: 750px;
    margin: auto;
    position: relative;
  }
  div#small-dialog iframe {
    width: 100%;
    height: 420px;
    display: block;
    border-radius: 10px;
  }
`

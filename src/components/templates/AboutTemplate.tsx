import { InstagramOutlined, TwitterOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { MouseEvent, useState } from "react";
import { NavLink } from "react-router-dom";
import { styled } from "styled-components";

const AboutTemplate = () => {
  const [activeNavLink, setActiveNavLink] = useState("");
  const addClass = (event: MouseEvent<HTMLAnchorElement>) => {
    const clickedNavLink = event.currentTarget.getAttribute("data-nav");
    setActiveNavLink(clickedNavLink);
  };
  return (
    <About style={{ marginTop: 100 }}>
      <div className="title">
        <h1>GIỚI THIỆU</h1>
      </div>
      <div className="otherPages">
        <ul>
          <li className="page1">
            <NavLink
              to={PATH.about}
              onClick={addClass}
              data-nav="main"
              className={activeNavLink === "main" ? "current" : ""}
            >
              Hệ thống Cyber
            </NavLink>
          </li>
          <li className="page2">
            <NavLink
              to={PATH.service}
              onClick={addClass}
              data-nav="services"
              className={activeNavLink === "services" ? "current" : ""}
            >
              Dịch vụ
            </NavLink>
          </li>
          <li className="page3">
            <NavLink
              to={PATH.tech}
              onClick={addClass}
              data-nav="technology"
              className={activeNavLink === "technology" ? "current" : ""}
            >
              Công nghệ phòng chiếu
            </NavLink>
          </li>
        </ul>
      </div>
      <div className="mainContent">
        <div className="content-item">
          <div className="content-pic">
            <img src="../../../images/hanoi.png" alt="" />
          </div>
          <div className="content-txt">
            <h3>CYBER MOVIE HÀ NỘI</h3>
            <p>
              Địa chỉ: Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Beatae vel libero dicta ipsam adipisci quos.
            </p>
            <p>Hotline: 102-252-9999</p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
        </div>
        <div className="content-item">
          <div className="content-txt" id="danang">
            <h3>CYBER MOVIE ĐÀ NẴNG</h3>
            <p>
              Địa chỉ: Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Beatae vel libero dicta ipsam adipisci quos.
            </p>
            <p>Hotline: 102-252-9999</p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img src="../../../images/danang.jpg" alt="" />
          </div>
        </div>
        <div className="content-item">
          <div className="content-pic">
            <img src="../../../images/hcm.jpg" alt="" />
          </div>
          <div className="content-txt">
            <h3>CYBER MOVIE TP.HỒ CHÍ MINH</h3>
            <p>
              Địa chỉ: Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Beatae vel libero dicta ipsam adipisci quos.
            </p>
            <p>Hotline: 102-252-9999</p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
        </div>
      </div>
      <div className="intro-note">
        <p id="intro">CYBER MOVIE, KHÔNG GIAN GIẢI TRÍ NĂNG ĐỘNG</p>
        <p id="welcome">
          Hãy đến CYBER MOVIE để trải nghiệm cảm giác điện ảnh đỉnh cao
        </p>
      </div>
    </About>
  );
};

export default AboutTemplate;
const About = styled.div`
  .title {
    text-align: center;
    margin-bottom: 20px;
    h1 {
      font-size: 2rem;
      font-family: "Futurab";
      font-weight: 600;
    }
  }
  .otherPages {
    ul {
      display: flex;
      justify-content: center;
      align-items: center;
      li {
        background: #f18720;
        height: 74px;
        line-height: 74px;
        box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
        border-top-left-radius: 30px;
        border-top-right-radius: 45px;
        border-bottom-right-radius: 45px;
        font-family: "avantgarde-demi";
        font-weight: normal;
        :hover {
          background: #fecf06;
          height: 74px;
          line-height: 74px;
          box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
          border-top-left-radius: 30px;
          border-top-right-radius: 45px;
          border-bottom-right-radius: 45px;
          color: black;
        }
        a {
          text-transform: uppercase;
          font-size: 30px;
          padding: 15px 60px 15px;
          color: white;
          font-weight: 500;
        }
        .current {
          background: #fecf06;
          height: 74px;
          line-height: 74px;
          box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
          border-top-left-radius: 30px;
          border-top-right-radius: 45px;
          border-bottom-right-radius: 45px;
          color: black;
        }
      }
      .page1 {
        transform: translateX(96px);
        z-index: 2;
      }
      .page2 {
        transform: translateX(48px);
      }
      .page3 {
        box-shadow: none;
      }
    }
  }
  .mainContent {
    position: relative;
    display: block;
    width: 90%;
    margin: 0 auto;
    max-width: 1200px;
    margin-bottom: 60px;
    .content-item {
      display: flex;
      align-items: center;
      margin-bottom: 30px;
      .content-pic {
        margin-top: 20px;
        width: 50%;
        img {
          width: 388px;
          height: 388px;
          border: 6px solid #fecf06;
          border-radius: 50%;
          box-shadow: 10px 10px 0 rgba(0, 0, 0, 0.3);
        }
      }
      .content-txt {
        margin-left: 30px;
        h3 {
          text-align: center;
          background-color: #e00d7a;
          color: white;
          padding: 30px;
          border-radius: 30px 30px 0px 0px;
          font-size: 26px;
          font-weight: 600;
          line-height: 30px;
        }
        p {
          font-weight: 500;
          font-size: 20px;
          font-family: "Times New Roman", Times, serif;
        }
      }
      #danang {
        margin-left: 0;
        margin-right: 30px;
      }
    }
  }
  .intro-note {
    text-align: center;
    #intro {
      margin-bottom: 40px;
      font-size: 25px;
      font-weight: 500;
      font-family: "Times New Roman", Times, serif;
    }
    #welcome {
      position: relative;
      font-size: 22px;
      font-family: "MyriadBold";
      font-style: italic;
    }
    #welcome::before {
      content: "";
      position: absolute;
      left: 220px;
      top: 0;
      width: 34px;
      height: 27px;
      background: url("https://www.cinestar.com.vn/catalog/view/theme/default/images/icon-welcome-bf.png");
    }
    #welcome::after {
      content: "";
      position: absolute;
      right: 220px;
      top: 15px;
      width: 34px;
      height: 27px;
      background: url("https://www.cinestar.com.vn/catalog/view/theme/default/images/icon-welcome-af.png");
    }
  }
`;

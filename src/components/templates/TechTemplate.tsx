import { InstagramOutlined, TwitterOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { MouseEvent, useState } from "react";
import { NavLink } from "react-router-dom";
import { styled } from "styled-components";

export const TechTemplate = () => {
  const [activeNavLink, setActiveNavLink] = useState("");
  const addClass = (event: MouseEvent<HTMLAnchorElement>) => {
    const clickedNavLink = event.currentTarget.getAttribute("data-nav");
    setActiveNavLink(clickedNavLink);
  };
  return (
    <Tech style={{ marginTop: 100 }}>
      <div className="title">
        <h1>CÔNG NGHỆ</h1>
      </div>
      <div className="otherPages">
        <ul>
          <li className="page1">
            <NavLink
              to={PATH.about}
              onClick={addClass}
              data-nav="main"
              className={activeNavLink === "main" ? "current" : ""}
            >
              Hệ thống Cyber
            </NavLink>
          </li>
          <li className="page2">
            <NavLink
              to={PATH.service}
              onClick={addClass}
              data-nav="services"
              className={activeNavLink === "services" ? "current" : ""}
            >
              Dịch vụ
            </NavLink>
          </li>
          <li className="page3">
            <NavLink
              to={PATH.tech}
              onClick={addClass}
              data-nav="technology"
              className={activeNavLink === "technology" ? "current" : ""}
            >
              Công nghệ phòng chiếu
            </NavLink>
          </li>
        </ul>
      </div>
      <div className="mainContent">
        <div className="content-item">
          <div className="content-txt">
            <div className="service-title">
              <h3>CÔNG NGHỆ 3D</h3>
            </div>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  So với công nghệ chiếu phim 2D Digital (Kỹ thuật số 2 chiều),
                  công nghệ 3D Digital (Kỹ thuật số 3 chiều) cho phép khán giả
                  cảm nhận thêm chiều sâu của hình ảnh, giúp cho không gian điện
                  ảnh trở nên sống động như không gian thực mà chúng ta đang
                  sống.
                </span>
              </span>
            </p>
            <p
              style={{
                fontSize: 20,
                fontFamily: "Times New Roman",
                marginTop: 20,
              }}
            >
              Phim 3D được quay từ tối thiểu hai máy cùng một lúc, từ hai góc
              nhìn khác nhau tương ứng với hoạt động của hai mắt người. Khi xem
              phim khán giả sẽ cần đeo kính 3D để lọc hình ảnh cho mỗi mắt, khi
              qua não bộ sẽ chập lại tạo thành hình ảnh không gian ba chiều.
            </p>
            <p
              style={{
                fontSize: 20,
                fontFamily: "Times New Roman",
                marginTop: 20,
              }}
            >
              Các phòng chiếu phim 3D Digital này đều sử dụng màn hình tráng bạc
              để giảm thiểu lượng hao hụt ánh sáng một cách tối đa.
            </p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img src="../../../images/3d.jpg" alt="" />
          </div>
        </div>
        <div className="content-item">
          <div className="content-txt">
            <div className="service-title">
              <h3>SWEETBOX</h3>
            </div>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Cyber movie có thể mang đến cho bạn những giây phút thư giãn
                  cùng những bộ phim hấp dẫn trong không gian nhẹ nhàng, ấm áp
                  cùng người yêu thương và gia đình. Hãy tận hưởng những giây
                  phút ngọt ngào nhất tại Cyber movie bằng ghế đôi SWEETBOX.
                </span>
              </span>
            </p>

            <p style={{ marginTop: 20 }}>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Với nỗ lực không ngừng mang đến cho người yêu phim Việt Nam
                  trải nghiệm điện ảnh tốt nhất, Cinestar mang đến loại ghế đôi
                  SWEETBOX cực kỳ độc đáo và mới lạ. SWEETBOX được đặt ở hàng
                  ghế cuối cùng trong phòng chiếu. Với vách ngăn cao cũng như sự
                  khéo léo trong thiết kế giấu đi tay gác chính giữa giúp cho
                  đôi bạn càng thêm gần gũi và ấm áp, tạo không gian hoàn hảo
                  cho các cặp đôi. Hãy đến và trải nghiệm không gian hoàn hảo
                  cho các cặp đôi với SWEETBOX.
                </span>
              </span>
            </p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img src="../../../images/sweetbox.png" alt="" />
          </div>
        </div>
        <div className="content-item">
          <div className="content-txt">
            <div className="service-title">
              <h3>DOLBY ATMOS</h3>
            </div>
            <p style={{ marginBottom: 20 }}>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Dolby Atmos – sự phát triển đáng kể nhất trong công nghệ âm
                  thanh kể từ âm thanh vòm, đang tạo ra sự thay đổi độc đáo
                  trong kĩ thuật thiết kế âm thanh phân lớp, hiện đã có mặt tại
                  Việt Nam và sẵn sàng phục vụ khán giả tại Cyber movie.
                </span>
              </span>
            </p>

            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Dolby Atmos sử dụng thiết kế phân lớp tân tiến để tạo nên các
                  rãnh âm thanh. Lớp nền bao gồm các dải âm thanh môi trường
                  tĩnh được phối theo phương pháp âm thanh phân luồng quen
                  thuộc. Các lớp trên trần bao gồm các yếu tố âm thanh động được
                  định hướng và thay đổi một cách chính xác theo hình ảnh hiển
                  thị trên màn hình trong rạp. Bằng cách lắp đặt hệ thống loa ở
                  trên đầu và xung quanh, Dolby Atmos có thể khiến khán giả trải
                  nghiệm những âm thanh trung thực và tự nhiên như thật của bộ
                  phim.
                </span>
              </span>
            </p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img src="../../../images/dolby.jpg" alt="" />
          </div>
        </div>
      </div>
      <div className="intro-note">
        <p id="intro">CYBER MOVIE, KHÔNG GIAN GIẢI TRÍ NĂNG ĐỘNG</p>
        <p id="welcome">
          Hãy đến CYBER MOVIE để trải nghiệm cảm giác điện ảnh đỉnh cao
        </p>
      </div>
    </Tech>
  );
};

export default TechTemplate;
const Tech = styled.div`
  .title {
    text-align: center;
    margin-bottom: 20px;
    h1 {
      font-size: 2rem;
      font-family: "Futurab";
      font-weight: 600;
    }
  }
  .otherPages {
    ul {
      display: flex;
      justify-content: center;
      align-items: center;
      li {
        background: #f18720;
        height: 74px;
        line-height: 74px;
        box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
        border-top-left-radius: 30px;
        border-top-right-radius: 45px;
        border-bottom-right-radius: 45px;
        font-family: "avantgarde-demi";
        font-weight: normal;
        :hover {
          background: #fecf06;
          height: 74px;
          line-height: 74px;
          box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
          border-top-left-radius: 30px;
          border-top-right-radius: 45px;
          border-bottom-right-radius: 45px;
          color: black;
        }
        a {
          text-transform: uppercase;
          font-size: 30px;
          padding: 15px 60px 15px;
          color: white;
          font-weight: 500;
        }
        .current {
          background: #fecf06;
          height: 74px;
          line-height: 74px;
          box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
          border-top-left-radius: 30px;
          border-top-right-radius: 45px;
          border-bottom-right-radius: 45px;
          color: black;
        }
      }
      .page1 {
        transform: translateX(96px);
        z-index: 2;
      }
      .page2 {
        transform: translateX(48px);
      }
      .page3 {
        box-shadow: none;
      }
    }
  }
  .mainContent {
    margin-top: 30px;
    .content-item {
      display: flex;
      background-color: rgba(68, 64, 64, 0.3);
      padding: 30px;
      border-radius: 20px;
      margin-bottom: 30px;
      .content-txt {
        width: 50%;
        .service-title {
          h3 {
            font-family: "Futurab";
            font-weight: normal;
            font-size: 36px;
            text-transform: uppercase;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
          }
        }
      }
      .content-pic {
        width: 50%;
        img {
          margin-top: 30px;
          margin-left: 30px;
          width: 450px;
          height: 300px;
          border-radius: 10px;
        }
      }
    }
  }
  .intro-note {
    margin-top: 50px;
    text-align: center;
    #intro {
      margin-bottom: 40px;
      font-size: 25px;
      font-weight: 500;
      font-family: "Times New Roman", Times, serif;
    }
    #welcome {
      position: relative;
      font-size: 22px;
      font-family: "MyriadBold";
      font-style: italic;
    }
    #welcome::before {
      content: "";
      position: absolute;
      left: 220px;
      top: 0;
      width: 34px;
      height: 27px;
      background: url("https://www.cinestar.com.vn/catalog/view/theme/default/images/icon-welcome-bf.png");
    }
    #welcome::after {
      content: "";
      position: absolute;
      right: 220px;
      top: 15px;
      width: 34px;
      height: 27px;
      background: url("https://www.cinestar.com.vn/catalog/view/theme/default/images/icon-welcome-af.png");
    }
  }
`;

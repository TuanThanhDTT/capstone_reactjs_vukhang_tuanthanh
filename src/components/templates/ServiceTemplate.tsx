import { InstagramOutlined, TwitterOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { MouseEvent, useState } from "react";
import { NavLink } from "react-router-dom";
import { styled } from "styled-components";

const ServiceTemplate = () => {
  const [activeNavLink, setActiveNavLink] = useState("");
  const addClass = (event: MouseEvent<HTMLAnchorElement>) => {
    const clickedNavLink = event.currentTarget.getAttribute("data-nav");
    setActiveNavLink(clickedNavLink);
  };
  return (
    <Service style={{ marginTop: 100 }}>
      <div className="title">
        <h1>DỊCH VỤ</h1>
      </div>
      <div className="otherPages">
        <ul>
          <li className="page1">
            <NavLink
              to={PATH.about}
              onClick={addClass}
              data-nav="main"
              className={activeNavLink === "main" ? "current" : ""}
            >
              Hệ thống Cyber
            </NavLink>
          </li>
          <li className="page2">
            <NavLink
              to={PATH.service}
              onClick={addClass}
              data-nav="services"
              className={activeNavLink === "services" ? "current" : ""}
            >
              Dịch vụ
            </NavLink>
          </li>
          <li className="page3">
            <NavLink
              to={PATH.tech}
              onClick={addClass}
              data-nav="technology"
              className={activeNavLink === "technology" ? "current" : ""}
            >
              Công nghệ phòng chiếu
            </NavLink>
          </li>
        </ul>
      </div>
      <div className="mainContent">
        <div className="content-item">
          <div className="content-txt">
            <div className="service-title">
              <h3>HỆ THỐNG CYBER BOWLING</h3>
            </div>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  <strong>1.&nbsp;Bowling Center</strong>
                </span>
              </span>
            </p>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Bowling Center có 10 lane bowling, phục vụ hàng trăm lượt
                  khách mỗi ngày, điểm đến giải trí, vận động lành mạnh cho mọi
                  lứa tuổi.
                </span>
              </span>
            </p>
            <p style={{ fontSize: 20, fontFamily: "Times New Roman" }}>
              Địa chỉ: Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Beatae vel libero dicta ipsam adipisci quos.
            </p>
            <p style={{ fontSize: 20, fontFamily: "Times New Roman" }}>
              Hotline: 102-252-9999
            </p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img src="../../../images/bowling.jpg" alt="" />
          </div>
        </div>
        <div className="content-item">
          <div className="content-txt">
            <div className="service-title">
              <h3>HỆ THỐNG NHÀ HÀNG CYBER RESTAURANT</h3>
            </div>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  <strong>2.&nbsp;Nhà hàng Món ngon Miền Tây</strong>
                </span>
              </span>
            </p>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Nhà hàng Món ngon Miền Tây có sức chứa 400 thực khách, phục vụ
                  hơn 200 món ăn khác nhau.
                </span>
              </span>
            </p>

            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Nhà hàng Món ngon hân hạnh phục vụ khách du lịch và địa
                  phương, mang đến trải nghiệm ẩm thực đa dạng các món ăn trong
                  nước và quốc tế.
                </span>
              </span>
            </p>
            <p style={{ fontSize: 20, fontFamily: "Times New Roman" }}>
              Địa chỉ: Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Beatae vel libero dicta ipsam adipisci quos.
            </p>
            <p style={{ fontSize: 20, fontFamily: "Times New Roman" }}>
              Hotline: 102-252-9999
            </p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img
              src="https://www.cinestar.com.vn/pictures/dich%20vu/monngon.jpg"
              alt=""
            />
          </div>
        </div>
        <div className="content-item">
          <div className="content-txt">
            <div className="service-title">
              <h3>HỆ THỐNG CYBER COFFEE</h3>
            </div>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  <strong>3.&nbsp;Coffee Shop</strong>
                </span>
              </span>
            </p>
            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  Nằm trong chuỗi các dự án được đầu tư tâm huyết, CYBER COFFEE
                  là một mô hình hoàn toàn mới khi có sự kết hợp giữa Rạp Chiếu
                  Phim và cửa hàng coffee shop - vừa sở hữu nét điện ảnh, vừa
                  đậm chất nghệ thuật thưởng thức cà phê. Với không gian mở &
                  bày trí hiện đại, bạn có thể dễ dàng lựa chọn một góc thư giãn
                  với nhiều loại thức uống.
                </span>
              </span>
            </p>

            <p>
              <span style={{ fontSize: 20 }}>
                <span style={{ fontFamily: "Times New Roman" }}>
                  CYBER COFFEE, Trải nghiệm tiện lợi - Thoải mái thư giãn, hứa
                  hẹn sẽ trở thành một nơi lý tưởng dành cho các bạn trẻ & giới
                  văn phòng.
                </span>
              </span>
            </p>
            <p style={{ fontSize: 20, fontFamily: "Times New Roman" }}>
              Địa chỉ: Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Beatae vel libero dicta ipsam adipisci quos.
            </p>
            <p style={{ fontSize: 20, fontFamily: "Times New Roman" }}>
              Hotline: 102-252-9999
            </p>
            <span style={{ fontSize: 25, marginRight: 10 }}>
              <InstagramOutlined />
            </span>
            <span>
              <TwitterOutlined style={{ fontSize: 25 }} />
            </span>
          </div>
          <div className="content-pic">
            <img src="../../../images/coffee.jpg" alt="" />
          </div>
        </div>
      </div>
      <div className="intro-note">
        <p id="intro">CYBER MOVIE, KHÔNG GIAN GIẢI TRÍ NĂNG ĐỘNG</p>
        <p id="welcome">
          Hãy đến CYBER MOVIE để trải nghiệm cảm giác điện ảnh đỉnh cao
        </p>
      </div>
    </Service>
  );
};

export default ServiceTemplate;
const Service = styled.div`
  .title {
    text-align: center;
    margin-bottom: 20px;
    h1 {
      font-size: 2rem;
      font-family: "Futurab";
      font-weight: 600;
    }
  }
  .otherPages {
    ul {
      display: flex;
      justify-content: center;
      align-items: center;
      li {
        background: #f18720;
        height: 74px;
        line-height: 74px;
        box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
        border-top-left-radius: 30px;
        border-top-right-radius: 45px;
        border-bottom-right-radius: 45px;
        font-family: "avantgarde-demi";
        font-weight: normal;
        :hover {
          background: #fecf06;
          height: 74px;
          line-height: 74px;
          box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
          border-top-left-radius: 30px;
          border-top-right-radius: 45px;
          border-bottom-right-radius: 45px;
          color: black;
        }
        a {
          text-transform: uppercase;
          font-size: 30px;
          padding: 15px 60px 15px;
          color: white;
          font-weight: 500;
        }
        .current {
          background: #fecf06;
          height: 74px;
          line-height: 74px;
          box-shadow: 10px 0 0 rgba(0, 0, 0, 0.1);
          border-top-left-radius: 30px;
          border-top-right-radius: 45px;
          border-bottom-right-radius: 45px;
          color: black;
        }
      }
      .page1 {
        transform: translateX(96px);
        z-index: 2;
      }
      .page2 {
        transform: translateX(48px);
      }
      .page3 {
        box-shadow: none;
      }
    }
  }
  .mainContent {
    margin-top: 30px;
    .content-item {
      display: flex;
      background-color: rgba(68, 64, 64, 0.3);
      padding: 30px;
      border-radius: 20px;
      margin-bottom: 30px;
      .content-txt {
        width: 50%;
        .service-title {
          h3 {
            font-family: "Futurab";
            font-weight: normal;
            font-size: 36px;
            text-transform: uppercase;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
          }
        }
      }
      .content-pic {
        width: 50%;
        img {
          margin-top: 30px;
          margin-left: 30px;
          width: 380px;
          height: auto;
          border-radius: 10px;
        }
      }
    }
  }
  .intro-note {
    margin-top: 50px;
    text-align: center;
    #intro {
      margin-bottom: 40px;
      font-size: 25px;
      font-weight: 500;
      font-family: "Times New Roman", Times, serif;
    }
    #welcome {
      position: relative;
      font-size: 22px;
      font-family: "MyriadBold";
      font-style: italic;
    }
    #welcome::before {
      content: "";
      position: absolute;
      left: 220px;
      top: 0;
      width: 34px;
      height: 27px;
      background: url("https://www.cinestar.com.vn/catalog/view/theme/default/images/icon-welcome-bf.png");
    }
    #welcome::after {
      content: "";
      position: absolute;
      right: 220px;
      top: 15px;
      width: 34px;
      height: 27px;
      background: url("https://www.cinestar.com.vn/catalog/view/theme/default/images/icon-welcome-af.png");
    }
  }
`;

import { PATH } from "constant";
import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { LoginSchema, LoginSchemaType } from "schema";
import { zodResolver } from "@hookform/resolvers/zod";
// import { quanLyNguoiDungServices } from "services";
import { toast } from "react-toastify";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyNguoiDung/thunk";
import { InputX } from "components/ui";
// import { InputX } from "ui";

export const LoginTemplate = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });
  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    // sử dụng Redux thunk
    dispatch(loginThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng nhập thành công!");
        navigate("/");
      });
  };
  return (
    <form className="pt-[30px] pb-[60px]" onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-white text-40">Sign In</h2>
      <div className="mt-40">
        <InputX
          register={register}
          name="taiKhoan"
          error={errors?.taiKhoan?.message}
        />
      </div>
      <div className="mt-40">
        <InputX
          register={register}
          name="matKhau"
          error={errors?.matKhau?.message}
        />
      </div>
      <div className="mt-40">
        <button className="text-white w-full bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">
          Sign In
        </button>
        <p className="mt-10 text-white">
          Chưa có tài khoản?
          <span
            className="text-blue-500 cursor-pointer ml-3"
            onClick={() => {
              navigate(PATH.register);
            }}
          >
            Đăng kí
          </span>
        </p>
      </div>
    </form>
  );
};

export default LoginTemplate;

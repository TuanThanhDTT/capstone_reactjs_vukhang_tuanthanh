import { Card, Carousel, Skeleton } from "components/ui";
import { PATH } from "constant";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { generatePath } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getUserThunk } from "store/quanLyNguoiDung/thunk";
import { getBannerListThunk, getMovieListThunk } from "store/quanLyPhim/thunk";
import { styled } from "styled-components";

const HomeTemplate = () => {
  const dispatch = useAppDispatch();
  const { movieList, isFetchingMovieList, bannerList } = useSelector(
    (state: RootState) => state.quanLyPhim
  );

  useEffect(() => {
    dispatch(getMovieListThunk());
    dispatch(getBannerListThunk());
    dispatch(getUserThunk());
  }, [dispatch]);

  const navigate = useNavigate();
  if (isFetchingMovieList) {
    return (
      <div className="grid grid-cols-4">
        {[...Array(16)].map((_, index) => {
          return (
            <Card key={index} className="!w-[350px]">
              <Skeleton.Image active className="!w-full !h-[250px]" />
              <Skeleton.Input active className="!w-full !mt-10" />
              <Skeleton.Input active className="!w-full !mt-10" />
            </Card>
          );
        })}
      </div>
    );
  }

  return (
    <Body>
      <CarouselX>
        <Carousel autoplay>
          {bannerList.map((banner, index) => {
            const path = generatePath(PATH.detail, { id: banner.maPhim });
            return (
              <div key={index}>
                <img
                  className="cursor-pointer carousel-img"
                  src={banner.hinhAnh}
                  alt={banner.maBanner.toLocaleString()}
                  onClick={() => navigate(path)}
                />
              </div>
            );
          })}
        </Carousel>
      </CarouselX>
      <div className="grid grid-cols-4 gap-30 mt-[30px]">
        {movieList?.map((movie, index) => {
          const path = generatePath(PATH.detail, { id: movie.maPhim });
          return (
            <div className="list-items mx-auto" key={index}>
              <div className="blogs__overlay">
                <NavLink to={path} className="btn btn_ticket">
                  Đặt vé
                </NavLink>
                <a
                  className="btn btn_view cursor-pointer"
                  onClick={() => {
                    navigate(path);
                  }}
                >
                  Chi tiết
                </a>
              </div>
              <Card
                hoverable
                style={{ width: 240, height: 470 }}
                cover={<img id="img_items" alt="example" src={movie.hinhAnh} />}
              >
                <Card.Meta
                  title={movie.tenPhim}
                  description={movie.moTa.substring(0, 50)}
                />
              </Card>
            </div>
          );
        })}
      </div>
    </Body>
  );
};

export default HomeTemplate;
const Body = styled.div`
  .list-items {
    position: relative;
  }
  .blogs__overlay {
    width: 240px;
    height: 360px;
    background: rgba(51, 51, 51, 0.7);
    border-radius: 5px;
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    z-index: 10;
    transition: 0.3s;
    overflow: hidden;
  }
  .list-items:hover .blogs__overlay {
    opacity: 1;
  }
  #img_items {
    width: 240px;
    height: 360px;
  }
  .btn {
    width: 170px;
    height: 50px;
    margin-bottom: 10px;
    border: 1px solid #d6bb3f;
    color: white;
    line-height: 50px;
    text-align: center;
    display: flex;
    flex-direction: column;
    transform: translate(35px, 120px);
    position: relative;
    transition: 0.8s;
  }
  .btn_ticket {
    top: -180px;
  }
  .btn_view {
    bottom: -180px;
  }
  .list-items:hover .btn_ticket {
    top: 0;
  }
  .list-items:hover .btn_view {
    bottom: 0;
  }
`;

const CarouselX = styled.div`
  padding-top: 20px;
  .carousel-img {
    height: 400px;
    width: 100vw;
  }
`;

import { CloseOutlined } from "@ant-design/icons";
import { Button } from "components/ui";
import { Fragment, useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { RootState, useAppDispatch } from "store";
import { quanLyDatVeActions } from "store/quanLyDatVe/slice";
import {
  getTicketsThunk,
  postMovieTicketsThunk,
} from "store/quanLyDatVe/thunk";
import { getUserThunk } from "store/quanLyNguoiDung/thunk";
import { styled } from "styled-components";
import { InfoChair, ListTicket } from "types";

export const TicketsTemPlate = () => {
  const params = useParams();
  const { maLichChieu } = params;
  const navigate = useNavigate();
  if (!localStorage.getItem("accessToken")) {
    navigate("/login");
  }
  const { thongTinPhim, danhSachGhe, chairBookings } = useSelector(
    (state: RootState) => state.quanLyDatVe
  );
  const { getUser } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getTicketsThunk(maLichChieu));
  }, [dispatch]);
  return (
    <Tickets className="grid grid-cols-12">
      <div className="col-span-9">
        <div className="screen">
          <div className="tele"></div>
          <div className="trapezoid">
            <h3>Màn Hình</h3>
          </div>
        </div>
        <div className="renderChairList">
          {danhSachGhe.map((chair, index) => {
            const classChairVip = chair.loaiGhe === "Vip" ? "chairVip" : "";
            const classChairBooked = chair.daDat === true ? "chairBooked" : "";
            let myBookingChair = "";
            //Khi người dùng đăng nhập thì dispatch thunk trả về thông tin user có thuộc tính tài khoản
            if (getUser.taiKhoan === chair.taiKhoanNguoiDat) {
              myBookingChair = "myBookingChair";
            }
            let classChairBooking = "";
            let indexChair = chairBookings.findIndex(
              (item) => item.maGhe === chair.maGhe
            );
            if (indexChair != -1) {
              classChairBooking = "chairBooking";
            }
            return (
              <Fragment key={index}>
                <Button
                  onClick={() => {
                    dispatch(quanLyDatVeActions.setChairBookings(chair));
                  }}
                  disabled={chair.daDat}
                  className={`chair ${classChairVip} ${classChairBooked} ${classChairBooking} ${myBookingChair}`}
                >
                  <p>
                    {" "}
                    {chair.daDat ? (
                      <CloseOutlined
                        style={{
                          fontSize: 16,
                        }}
                      />
                    ) : (
                      chair.stt
                    )}
                  </p>
                </Button>
                {(index + 1) % 16 === 0 ? <br /> : ""}
              </Fragment>
            );
          })}
        </div>
        <div className="mt-10 flex justify-center">
          <table className="divide-y divide-gray-200 w-2/3">
            <thead className="bg-gray-50 p-5">
              <tr>
                <th>Ghế trống</th>
                <th>Ghế Vip</th>
                <th>Ghế đang đặt</th>
                <th>Ghế mình đặt</th>
                <th>Ghế đã được đặt</th>
              </tr>
            </thead>
            <tbody className="text-center">
              <tr>
                <td>
                  <span className="chair-1">
                    <Button></Button>
                  </span>
                </td>
                <td>
                  <span className="chair-1 chair-3">
                    <Button></Button>
                  </span>
                </td>
                <td>
                  <span className="chair-1 chair-4">
                    <Button></Button>
                  </span>
                </td>
                <td>
                  <span className="chair-1 chair-5">
                    <Button></Button>
                  </span>
                </td>
                <td>
                  <span className="chair-1 chair-2 mr-5">
                    <Button>X</Button>
                  </span>
                  <span className="chair-1 chair-3">
                    <Button>X</Button>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className="col-span-3 main-ticket">
        <h3 className="text-center text-2xl">{thongTinPhim?.tenPhim}</h3>
        <hr />
        <div className="grid grid-cols-2 content">
          <div>Ngày chiếu giờ chiếu</div>
          <div className="">
            {thongTinPhim?.ngayChieu}-{thongTinPhim?.gioChieu}
          </div>
        </div>
        <hr />
        <div className="grid grid-cols-2 content">
          <div>Cụm rạp</div>
          <div className="">{thongTinPhim?.tenCumRap}</div>
        </div>
        <hr />
        <div className="grid grid-cols-2 content">
          <div>Rạp</div>
          <div className="">{thongTinPhim?.tenRap}</div>
        </div>
        <hr />
        <div className="grid grid-cols-2 content">
          <div>Ghế chọn</div>
          <div>
            {chairBookings.map((item, index) => (
              <Fragment key={index}>
                <span key={index} style={{ marginRight: 5 }}>
                  {item.tenGhe}
                </span>
                {(index + 1) % 4 === 0 ? <br /> : ""}
              </Fragment>
            ))}
          </div>
        </div>
        <hr />
        <div className="grid grid-cols-2 content">
          <div>Ưu đãi</div>
          <div className="">0%</div>
        </div>
        <hr />
        <div className="grid grid-cols-2 content">
          <div>Tổng tiền</div>
          <div className="">
            {chairBookings
              .reduce((tongTien, chair) => {
                return (tongTien += chair.giaVe);
              }, 0)
              .toLocaleString()}
            {" VND"}
          </div>
        </div>
        <hr />
        <div className="button-booking">
          <Button
            onClick={() => {
              const infoTickets: InfoChair<ListTicket> = {
                maLichChieu: thongTinPhim.maLichChieu,
                danhSachVe: chairBookings,
              };
              dispatch(postMovieTicketsThunk(infoTickets))
                .unwrap()
                .then(() => {
                  toast.success("Đặt vé thành công!");
                  dispatch(getTicketsThunk(maLichChieu));
                  dispatch(quanLyDatVeActions.hoanTat([]));
                  dispatch(getUserThunk());
                  // navigate("/");
                });
            }}
          >
            BOOKING TICKET
          </Button>
        </div>
      </div>
    </Tickets>
  );
};

export default TicketsTemPlate;
const Tickets = styled.div`
  height: 650px;
  margin-top: 80px;
  padding: 10px;
  background-image: url("../../../images/bgmovie.jpg");
  background-position: center;
  .screen {
    display: flex;
    flex-direction: column;
    align-items: center;
    .tele {
      background: black;
      width: 80%;
      height: 15px;
    }
    .trapezoid {
      border-bottom: 50px solid white;
      border-left: 25px solid transparent;
      border-right: 25px solid transparent;
      height: 0;
      width: 80%;
      opacity: 0.6;
      box-shadow: 0px 35px 43px -2px rgba(174, 163, 163, 0.75);
      h3 {
        margin-top: 10px;
        text-align: center;
        font-weight: bold;
      }
    }
  }
  .renderChairList {
    margin-top: 10px;
    margin-left: 80px;
    .chair {
      width: 35px;
      height: 35px;
      border: none;
      border-radius: 5px;
      cursor: pointer;
      margin: 5px;
      background-color: rgba(53, 51, 52, 0.842);
      color: white;
      font-size: 15px;
      position: relative;
      p {
        /* transform: translateX(-5px); */
        position: absolute;
        left: 10px;
        bottom: 5px;
      }
    }
    .chairBooked {
      background-color: #cf0909;
      cursor: no-drop;
    }
    .chairBooking {
      background-color: #14ee14 !important;
    }
    .chairVip {
      background-color: #e28c09e0;
    }
    .myBookingChair {
      box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
      color: orange;
      border: 1px solid black;
      background: white;
    }
  }
  table {
    background: rgba(74, 223, 131, 0.6);
    .chair-1 {
      .ant-btn-default {
        width: 35px;
        height: 35px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        margin: 5px;
        background-color: rgba(53, 51, 52, 0.842);
        color: white;
        font-size: 15px;
        position: relative;
        border: 1px solid black;
        span {
          /* transform: translateX(-5px); */
          position: absolute;
          left: 13px;
          bottom: 5px;
        }
      }
    }
    .chair-2 {
      .ant-btn-default {
        background-color: #cf0909;
        cursor: no-drop;
        border: 1px solid black;
      }
    }
    .chair-3 {
      .ant-btn-default {
        background-color: #e28c09e0;
        border: 1px solid black;
      }
    }
    .chair-4 {
      .ant-btn-default {
        background-color: #14ee14;
        border: 1px solid black;
      }
    }
    .chair-5 {
      .ant-btn-default {
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        color: orange;
        border: 1px solid black;
        background: white;
      }
    }
  }
  .main-ticket {
    margin-top: 15px;
    height: 580px;
    background: rgba(25, 1, 26, 0.6);
    box-shadow: rgba(248, 247, 248, 0.9) 0px 5px 15px;
    border-radius: 10px;
    color: white;
    padding: 10px;
    hr {
      height: 3px;
    }
    h3 {
      padding: 10px 0;
      color: #14ee14;
    }
    .content {
      padding: 15px 10px;
    }
    .button-booking {
      padding: 10px 0;
      text-align: center;
      button {
        margin: 10px auto;
        padding: 10px 30px;
        border-radius: 5px;
        background: rgb(15, 122, 136);
        transition: 0.5s;
        span {
          position: relative;
          top: -6px;
          color: white;
          transition: 0.5s;
        }
      }
      :hover {
        background: rgb(5, 177, 28);
        border-color: white !important;
      }
    }
  }
`;

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm, SubmitHandler } from "react-hook-form";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { RegisterSchema, RegisterSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";
import { PATH } from "constant";
export const RegisterTemplate = () => {
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema),
  });

  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    try {
      await quanLyNguoiDungServices.register(value);
      toast.success("Đăng kí thành công");
      navigate(PATH.login);
    } catch (err) {
      toast.error(err?.response?.data?.content);
      console.log(err);
    }
  };
  return (
    <form className="pt-[10px] pb-[20px]" onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-white text-40 font-600">Register</h2>
      <div className="mt-20">
        <input
          type="text"
          placeholder="Tài khoản"
          className="outline-none block w-full p-8 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          // {...register('taikhoan', {
          //     required: 'Vui lòng nhập tài khoản',
          //     minLength: {
          //         value:6,
          //         message: 'Nhập từ 6 kí tự'
          //     },
          //     maxLength: {
          //         value: 20,
          //         message: "Nhập tối đa 20 kí tự"
          //     },
          // })}
          {...register("taiKhoan")}
        />
        <p className="text-red-500">{errors?.taiKhoan?.message as string}</p>
      </div>
      <div className="mt-20">
        <input
          type="password"
          placeholder="Mật khẩu"
          className="outline-none block w-full p-8 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("matKhau")}
        />
        <p className="text-red-500">{errors?.matKhau?.message as string}</p>
      </div>
      <div className="mt-20">
        <input
          type="text"
          placeholder="Email"
          className="outline-none block w-full p-8 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("email")}
        />
        <p className="text-red-500">{errors?.email?.message as string}</p>
      </div>
      <div className="mt-20">
        <input
          type="text"
          placeholder="Số điện thoại"
          className="outline-none block w-full p-8 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("soDt")}
        />
        <p className="text-red-500">{errors?.soDt?.message as string}</p>
      </div>
      <div className="mt-20">
        <input
          type="text"
          placeholder="Mã nhóm"
          className="outline-none block w-full p-8 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("maNhom")}
        />
        <p className="text-red-500">{errors?.maNhom?.message as string}</p>
      </div>
      <div className="mt-20">
        <input
          type="text"
          placeholder="Họ và tên"
          className="outline-none block w-full p-8 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("hoTen")}
        />
        <p className="text-red-500">{errors?.hoTen?.message as string}</p>
      </div>
      <div className="mt-20">
        <button className="text-white w-full bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">
          Register
        </button>
      </div>
    </form>
  );
};

export default RegisterTemplate;

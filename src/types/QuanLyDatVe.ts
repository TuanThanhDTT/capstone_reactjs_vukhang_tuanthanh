export interface Tickets<T,G>{
    thongTinPhim: T,
    danhSachGhe: G[],
}

export type MovieInfo = {
    maLichChieu: string,
    tenCumRap: string,
    tenRap: string,
    diaChi: string,
    tenPhim: string,
    hinhAnh: string,
    ngayChieu:string,
    gioChieu: string,
}
export type ChairList = {
    maGhe: number,
    tenGhe: string,
    maRap: number,
    loaiGhe: string,
    stt: string,
    giaVe: number,
    daDat: boolean,
    taiKhoanNguoiDat: null,
}

export interface InfoChair<T>{
    maLichChieu: string,
    danhSachVe: T[]
}
export type ListTicket = {
    maGhe: number,
    giaVe: number
}

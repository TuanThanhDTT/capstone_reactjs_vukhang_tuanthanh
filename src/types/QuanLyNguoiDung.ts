export type User = {
  taiKhoan: string;
  hoTen: string;
  email: string;
  soDT: string;
  maNhom: string;
  maLoaiNguoiDung: string;
  accessToken: string;
};
export type UserInfo<T> = User & {
  loaiNguoiDung: {
    maLoaiNguoiDung: string;
    tenLoai: string;
  };
  thongTinDatVe: T[];
};

export type TicketInfo<G> = {
  danhSachGhe: G[],
  giaVe : number,
  hinhAnh: string,
  maVe: number,
  ngayDat: string,
  tenPhim: string,
  thoiLuongPhim: number
}
export type ChairListUser = {
  maCumRap: string,
  maGhe: number,
  maHeThongRap:string,
  maRap:number,
  tenCumRap: string,
  tenGhe:string,
  tenHeThongRap:string,
  tenRap:string,
}

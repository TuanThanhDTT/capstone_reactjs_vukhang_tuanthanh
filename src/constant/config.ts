export const PATH = {
    demo: '/demo',
    home: '/',
    login: '/login',
    register: '/register',
    logout: '/logout',
    account: '/account',
    about: '/about',
    contact: '/contact',
    service: '/about/service',
    tech: '/about/tech',
    ticket: '/ticket/:maLichChieu',
    detail: '/detail/:id',
}